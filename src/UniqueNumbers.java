import java.util.Scanner;

public class UniqueNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the size of array");
		int size = scan.nextInt();
		int arr[] = new int[size];
		System.out.println("the elements are");
		for (int i = 0; i < size; i++) {
			arr[i] = scan.nextInt();
		}
		int temp[] = findUnique(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.println(temp[i]);
		}
	}

	public static int[] findUnique(int arr[]) {
		int count = 0;
		int k = 0;
		int result[] = new int[arr.length];
		for (int i = 0; i < arr.length; i++) {
			count = 0;
			for (int j = 0; j < arr.length; j++) {
				if (arr[i] == arr[j])
					count++;
			}
			if (count == 1)
				// System.out.println(arr[i]);
				result[k++] = arr[i];
		}
		return result;
	}
}
