package javaprograms;

import java.util.Scanner;

public class SpeedMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		int hour, min, sec;
		double speedInMps, speedInKmph, speedInMlph;
		
		System.out.println("enter the distance");
		int distanceInMeters = scan.nextInt();
		
		System.out.println("enter the time values");
		hour = scan.nextInt();
		min = scan.nextInt();
		sec = scan.nextInt();

		System.out.println(hour + "h" + min + "m" + sec + "s");
	
		int totalTimeInSec = timeSec(hour, min, sec);
		
		System.out.println(timeSec(1, 2 , 30));
		
		speedInMps = speedMs(distanceInMeters, totalTimeInSec);
		System.out.println("speed in m/s" + speedInMps);
		
		speedInKmph = speedKmHr(distanceInMeters, totalTimeInSec);
		System.out.println("speed in km/hr" + speedInKmph);
		
		speedInMlph = speedMihr(distanceInMeters, totalTimeInSec);
		System.out.println("speed in mi/hr" + speedInMlph);
	}

	public static int timeSec(int p1, int p2, int p3) {
		int time = (p1 * 60 * 60) + (p2 * 60) + (p3);
		return time;
	}

	public static double speedMs(double dis, int time) {

		double result = dis / time;
		return result;
	}

	public static double speedKmHr(double dis, int time) {

		double result = (dis / time) * (18 / 5);
		return result;
	}

	public static double speedMihr(double dis, int time) {

		double result = (dis / time) * (3600 / 1609);
		return result;
	}

}
