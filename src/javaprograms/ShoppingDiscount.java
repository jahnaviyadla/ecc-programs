package javaprograms;

import java.util.Scanner;

public class ShoppingDiscount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double sp = 0;
		Scanner scan = new Scanner(System.in);
		System.out.println("enter the price of items");
		double cp = scan.nextDouble();
		display(cp, sp);
	}

	public static void display(double cp, double sp) {
		if (cp < 10000) {
			System.out.println("you get the discount of 10%");
			
			double result = cp * 10 / 100;
			sp = cp - result;
			
			System.out.println("the selling prise is" + sp);
		
		} else if (cp <= 20000) {
			System.out.println("you get the discount of 20%");
			
			double result = cp * 20 / 100;
			sp = cp - result;
			
			System.out.println("the selling prise is" + sp);

		} else {

			System.out.println("you get the discount of 25%");
			
			double result = cp * 25 / 100;
			sp = cp - result;
			
			System.out.println("the selling prise is" + sp);
		}
	}
}
