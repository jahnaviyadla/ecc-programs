package PrimePrograms;

import java.util.Scanner;

public class NextPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		getPalindrome(num);

	}

	public static int reverse(int num) {
		int rev_num = 0;
		while (num > 0) {
			int r = num % 10;
			rev_num = (rev_num * 10) + r;
			num = num / 10;
		}
		return rev_num;

	}

	public static void getPalindrome(int num) {
		int count = 0;
		int i = 0;
		for (i = num + 1; i > num; i++) {
			if (i ==reverse(i)) {
				count++;
				break;
			}

		}
		if (count == 1)
			System.out.println(i);

	}

}
