package LoopsPrograms;

import java.util.Scanner;

public class CombinationProblem {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float bpr = 0.5f, opr = 1, apr = 5;
		getFruitCombo(bpr, opr, apr);

	}

	public static void getFruitCombo(float bpr, float opr, float apr) {
		for (int banana = 1; banana <= 100; banana++) {
			for (int orange = 1; orange <= 100; orange++) {
				for (int apple = 1; apple <= 100; apple++) {
					if ((banana + orange + apple == 100) && (banana * bpr + orange * opr + apple * apr == 100)) {
						System.out.println("banana :" + banana + "orange:" + orange + "apple :" + apple);
					}

				}
			}
		}
	}
}
