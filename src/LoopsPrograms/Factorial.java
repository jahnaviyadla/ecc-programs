package LoopsPrograms;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		int f = FactNum(num);
		System.out.println(f);

	}

	public static int FactNum(int num) {
		int i = 1;
		int f= 1;
		for (i = 1; i <= num; i++) {
			f = f * i;
		}
		return f;
	}

}
