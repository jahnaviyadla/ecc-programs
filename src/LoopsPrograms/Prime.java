package LoopsPrograms;

import java.util.Scanner;

public class Prime {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		boolean s = PrimeNum(num);
		System.out.println(s);

	}

	public static boolean PrimeNum(int num) {

		int count = 0;
		for (int i = 1; i <= num; i++) {
			if (num % i == 0) {
				count++;
				System.out.println(i);
			}

		}
		if (count == 2)
			return true;
		else
			return false;

	}

	public static boolean isprime(int n) {
		int flag = 0;
		for (int i = 2; i < n / 2; i++)
			if (n % i == 0) {
				flag = 1;
				break;
			}

		if (flag == 1)
			return false;
		else
			return true;
	}
}