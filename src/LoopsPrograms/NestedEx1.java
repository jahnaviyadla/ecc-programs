package LoopsPrograms;

public class NestedEx1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int stars = 5;
		for(int  row = 1; row <= stars; row++) {
		  for (int col = 1; col <= row; col++) {
		    System.out.print("* ");
		  }
		  System.out.print("\n");
		}

	}

}
