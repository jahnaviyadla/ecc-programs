package LoopsPrograms;

import java.util.Scanner;

public class SecondMax {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		secMaxDigit(num);

	}

	public static void secMaxDigit(int num) {
		int max1 = 0, max2 = 0, digit = 0, rem;
		while (num > 0) {
			digit = num % 10;
			if (digit > max1) {
				max2 = max1;
				max1 = digit;
			} else {
				/*
				 * num = num / 10; rem = num % 10;
				 */if (digit > max2 && digit != max1) {
					max2 = digit;

				}
			}
			num = num / 10;

		}
		System.out.println(max1);
		System.out.println(max2);
	}

}
