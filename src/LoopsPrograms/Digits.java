package LoopsPrograms;

import java.util.Scanner;

public class Digits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("enter a number");
		int num = scan.nextInt();
		getDigit(num);
	}

	public static void getDigit(int num) {
		int i = 1;
		int digit = 0;
		while (num >= i) {
			digit = num % 10;
			num = num / 10;
			System.out.println(digit);
		}

	}
}
