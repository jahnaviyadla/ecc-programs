package LoopsPrograms;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		System.out.println("enter a number");
		int num=scan.nextInt();
		int r=reverse(num);
		System.out.println("the reverse is"+r);

	}
   public static int reverse(int num){
	   int rev_num=0;
	   int r;
	   while(num>0){
		    r=num%10;
		    rev_num=rev_num*10+r;
		    num=num/10;
		     }
	   return rev_num;
   }
}
